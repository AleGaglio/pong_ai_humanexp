# Pong_AI_HumanEXP

![](images/Pong.png)

## Requirements
*  Python == 3.6
*  Jupyter

## Settings

### Clone repository
`git clone https://gitlab.com/AleGaglio/pong_ai_humanexp`

### Installing the requirements
`pip install -r Train/requirements.txt`

### Import ROMs
`python -m retro.import .`


## Train and Play

### Training
Run 'PongAtari2600_Retro.ipynb'

**EDITABLE PARAMETERS**:
*     loading: (bool) #to load a checkpoint file   
*     prioritizedMemory: (bool) #to use a memory with prioritized experience
*     human_exp: (bool) #to load human experience before training 

### Playing
Run 'MainMenu_Pong.py'

![](images/Schermata_2020-05-02_alle_18.11.19.png)

*  To play standard pong:
    Select 'CPU' as an opponent and press 'Let's Play!'

*  To record human experience:
    Select 'CPU' as an opponent, 'Yes' on Recording and press 'Let's Play!'

*  To play versus AI:
    Select 'AI' as an opponent, 'Yes' on Training (if you want train the agent in real time),
    'Yes' on Recording (if you want record the game) and press 'Let's Play!' 
