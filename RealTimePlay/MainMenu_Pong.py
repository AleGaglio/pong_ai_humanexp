import versusAI as AI
import sys

sys.path.insert(0, '../../')

import os
import pygame
import pygameMenu


HELP = ['Press W to move UP',
        'Press S to move DOWN',
        pygameMenu.locals.TEXT_NEWLINE,
        pygameMenu.locals.TEXT_NEWLINE,

         'Author: @{0}'.format('AleGaglio'),
         'Email: {0}'.format('a.gagliolo@campus.unimib.it')]
COLOR_BLACK = (0, 0, 0)
COLOR_WHITE = (255, 255, 255)
FPS = 60.0
MENU_BACKGROUND_COLOR = (228, 100, 36)
WINDOW_SIZE = (640, 480)

sound = None

# noinspection PyTypeChecker
surface = None  # type: pygame.SurfaceType

# noinspection PyTypeChecker
main_menu = None  # type: pygameMenu.Menu

# -----------------------------------------------------------------------------
# Methods
# -----------------------------------------------------------------------------
def main_background():
    """
    Background color of the main menu, on this function user can plot
    images, play sounds, etc.
    :return: None
    """
    global surface
    surface.fill((40, 40, 40))


def check_name_test(value):
    """
    This function tests the text input widget.
    :param value: The widget value
    :type value: basestring
    :return: None
    """
    print('User name: {0}'.format(value))


# noinspection PyUnusedLocal
def update_menu_sound(value, enabled):
    """
    Update menu sound.
    :param value: Value of the selector (Label and index)
    :type value: tuple
    :param enabled: Parameter of the selector, (True/False)
    :type enabled: bool
    :return: None
    """
    global main_menu
    global sound
    if enabled:
        main_menu.set_sound(sound, recursive=True)
        print('Menu sound were enabled')
    else:
        main_menu.set_sound(None, recursive=True)
        print('Menu sound were disabled')


def main(test=False):
    """
    Main program.
    :param test: Indicate function is being tested
    :type test: bool
    :return: None
    """

    # -------------------------------------------------------------------------
    # Globals
    # -------------------------------------------------------------------------
    global main_menu
    global sound
    global surface

    # -------------------------------------------------------------------------
    # Init pygame
    # -------------------------------------------------------------------------
    pygame.init()
    os.environ['SDL_VIDEO_CENTERED'] = '1'

    # Create pygame screen and objects
    surface = pygame.display.set_mode(WINDOW_SIZE)
    pygame.display.set_caption('Atari Pong')
    clock = pygame.time.Clock()

    # -------------------------------------------------------------------------
    # Create menus
    # -------------------------------------------------------------------------


    help_menu = pygameMenu.TextMenu(surface,
                                bgfun=main_background,
                                color_selected=COLOR_WHITE,
                                font=pygameMenu.font.FONT_MUNRO,
                                font_color=COLOR_BLACK,
                                font_size=30,
                                font_size_title=40,
                                menu_alpha=100,
                                menu_color=MENU_BACKGROUND_COLOR,
                                menu_height=int(WINDOW_SIZE[1] * 0.7),
                                menu_width=int(WINDOW_SIZE[0] * 0.8),
                                # User press ESC button
                                onclose=pygameMenu.events.DISABLE_CLOSE,
                                option_shadow=False,
                                title='Help',
                                window_height=WINDOW_SIZE[1],
                                window_width=WINDOW_SIZE[0]
                                )

    for line in HELP:
        help_menu.add_line(line)

    help_menu.add_button('Return to Main Menu', pygameMenu.events.BACK)

    def get_param():
        data = main_menu.get_input_data()

        op = data['opponent'][0]
        train = data['train'][0]
        record = data['record'][0]

        if train == 'No':
            train = False
        else:
            train = True

        if record == 'No':
            record = False
        else:
            record = True

        pygameMenu.events.CLOSE

        AI.run(op, train, record)


    # Main menu
    main_menu = pygameMenu.Menu(surface,
                                bgfun=main_background,
                                color_selected=COLOR_WHITE,
                                font=pygameMenu.font.FONT_MUNRO,
                                font_color=COLOR_BLACK,
                                font_size=30,
                                font_size_title=40,
                                menu_alpha=100,
                                menu_color=MENU_BACKGROUND_COLOR,
                                menu_height=int(WINDOW_SIZE[1] * 0.7),
                                menu_width=int(WINDOW_SIZE[0] * 0.8),
                                # User press ESC button
                                onclose=pygameMenu.events.EXIT,
                                option_shadow=False,
                                title='Atari Pong',
                                window_height=WINDOW_SIZE[1],
                                window_width=WINDOW_SIZE[0]
                                )
    main_menu.set_fps(FPS)

    main_menu.add_selector('Select opponent',
                               [('Cpu', 'CPU'),
                                ('AI', 'AI')],
                               selector_id='opponent',
                           default=1)

    main_menu.add_selector('Training',
                               [('Yes', True),
                                ('No', False)],
                               selector_id='train',
                               default=1)

    main_menu.add_selector('Recording',
                               [('Yes', True),
                                ('No', False)],
                               selector_id='record',
                               default=1)
    main_menu.add_button('Let\'s play!', get_param)

    main_menu.add_button('Help', help_menu)

    main_menu.add_button('Quit', pygameMenu.events.EXIT)

    # -------------------------------------------------------------------------
    # Main loop
    # -------------------------------------------------------------------------
    while True:

        # Tick
        clock.tick(FPS)

        # Paint background
        main_background()

        # Main menu
        main_menu.mainloop(disable_loop=test)

        # Flip surface
        pygame.display.flip()

        # At first loop returns
        if test:
            break

if __name__ == '__main__':
    main()