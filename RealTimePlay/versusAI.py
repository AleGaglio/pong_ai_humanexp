
from tqdm import tqdm
import retro
import torch
import torch.nn as nn
import torch.optim as optim
import time
import random, math
from IPython.display import clear_output
import pygame
from pygame.locals import VIDEORESIZE
from gym import logger
import play as cpu_game

"""#Retro_wrapper"""

from collections import deque
import cv2

cv2.ocl.setUseOpenCL(False)
# from .atari_wrappers import WarpFrame, ClipRewardEnv, FrameStack, ScaledFloatFrame
# from .wrappers import TimeLimit
import numpy as np
import gym
from gym.wrappers.time_limit import TimeLimit
from matplotlib import pyplot as plt


class LazyFrames(object):
    def __init__(self, frames):
        """This object ensures that common frames between the observations are only stored once.
        It exists purely to optimize memory usage which can be huge for DQN's 1M frames replay
        buffers.
        This object should only be converted to numpy array before being passed to the model.
        You'd not believe how complex the previous solution was."""
        self._frames = frames
        self._out = None

    def _force(self):
        if self._out is None:
            self._out = np.concatenate(self._frames, axis=-1)
            self._frames = None
        return self._out

    def __array__(self, dtype=None):
        out = self._force()
        if dtype is not None:
            out = out.astype(dtype)
        return out

    def __len__(self):
        return len(self._force())

    def __getitem__(self, i):
        return self._force()[i]


class ImageToPyTorch(gym.ObservationWrapper):
    """
    Image shape to num_channels x weight x height
    """

    def __init__(self, env):
        super(ImageToPyTorch, self).__init__(env)
        old_shape = self.observation_space.shape
        self.observation_space = gym.spaces.Box(low=0.0, high=1.0, shape=(old_shape[-1], old_shape[0], old_shape[1]),
                                                dtype=np.uint8)

    def observation(self, observation):
        return np.swapaxes(observation, 2, 0)


class FrameStack(gym.Wrapper):
    def __init__(self, env, k):
        """Stack k last frames.
        Returns lazy array, which is much more memory efficient.
        See Also
        --------
        baselines.common.atari_wrappers.LazyFrames
        """
        gym.Wrapper.__init__(self, env)
        self.k = k
        self.frames = deque([], maxlen=k)
        shp = env.observation_space.shape
        self.observation_space = gym.spaces.Box(low=0, high=255, shape=(shp[:-1] + (shp[-1] * k,)),
                                                dtype=env.observation_space.dtype)

    def reset(self):
        ob = self.env.reset()
        for _ in range(self.k):
            self.frames.append(ob)
        return self._get_ob()

    def step(self, action):
        ob, reward, done, info = self.env.step(action)
        self.frames.append(ob)
        return self._get_ob(), reward, done, info

    def _get_ob(self):
        assert len(self.frames) == self.k
        return LazyFrames(list(self.frames))


class ScaledFloatFrame(gym.ObservationWrapper):
    def __init__(self, env):
        gym.ObservationWrapper.__init__(self, env)
        self.observation_space = gym.spaces.Box(low=0, high=1, shape=env.observation_space.shape, dtype=np.float32)

    def observation(self, observation):
        # careful! This undoes the memory optimization, use
        # with smaller replay buffers only.
        return np.array(observation).astype(np.float32) / 255.0


class WarpFrame(gym.ObservationWrapper):
    def __init__(self, env, width=84, height=84, grayscale=True):
        """Warp frames to 84x84 as done in the Nature paper and later work."""
        gym.ObservationWrapper.__init__(self, env)
        self.width = width
        self.height = height
        self.grayscale = grayscale
        if self.grayscale:
            self.observation_space = gym.spaces.Box(low=0, high=255,
                                                    shape=(self.height, self.width, 1), dtype=np.uint8)
        else:
            self.observation_space = gym.spaces.Box(low=0, high=255,
                                                    shape=(self.height, self.width, 3), dtype=np.uint8)

    def observation(self, frame):
        if self.grayscale:
            frame = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
        frame = cv2.resize(frame, (self.width, self.height), interpolation=cv2.INTER_AREA)
        if self.grayscale:
            frame = np.expand_dims(frame, -1)
        return frame


class ClipRewardEnv(gym.RewardWrapper):
    def __init__(self, env):
        gym.RewardWrapper.__init__(self, env)

    def reward(self, reward):
        """Bin reward to {+1, 0, -1} by its sign."""
        return np.sign(reward)


class StochasticFrameSkip(gym.Wrapper):
    def __init__(self, env, n, stickprob):
        gym.Wrapper.__init__(self, env)
        self.n = n
        self.stickprob = stickprob
        self.curac = None
        self.rng = np.random.RandomState()
        self.supports_want_render = hasattr(env, "supports_want_render")

    def reset(self, **kwargs):
        self.curac = None
        return self.env.reset(**kwargs)

    def step(self, ac):
        done = False
        totrew = 0
        for i in range(self.n):
            # First step after reset, use action
            if self.curac is None:
                self.curac = ac
            # First substep, delay with probability=stickprob
            elif i == 0:
                if self.rng.rand() > self.stickprob:
                    self.curac = ac
            # Second substep, new action definitely kicks in
            elif i == 1:
                self.curac = ac
            if self.supports_want_render and i < self.n - 1:
                ob, rew, done, info = self.env.step(self.curac, want_render=False)
            else:
                ob, rew, done, info = self.env.step(self.curac)
            totrew += rew[0]
            if done: break
        return ob, totrew, done, info

    def seed(self, s):
        self.rng.seed(s)


class PartialFrameStack(gym.Wrapper):
    def __init__(self, env, k, channel=1):
        """
        Stack one channel (channel keyword) from previous frames
        """
        gym.Wrapper.__init__(self, env)
        shp = env.observation_space.shape
        self.channel = channel
        self.observation_space = gym.spaces.Box(low=0, high=255,
                                                shape=(shp[0], shp[1], shp[2] + k - 1),
                                                dtype=env.observation_space.dtype)
        self.k = k
        self.frames = deque([], maxlen=k)
        shp = env.observation_space.shape

    def reset(self):
        ob = self.env.reset()
        assert ob.shape[2] > self.channel
        for _ in range(self.k):
            self.frames.append(ob)
        return self._get_ob()

    def step(self, ac):
        ob, reward, done, info = self.env.step(ac)
        self.frames.append(ob)
        return self._get_ob(), reward, done, info

    def _get_ob(self):
        assert len(self.frames) == self.k
        return np.concatenate([frame if i == self.k - 1 else frame[:, :, self.channel:self.channel + 1]
                               for (i, frame) in enumerate(self.frames)], axis=2)


class Downsample(gym.ObservationWrapper):
    def __init__(self, env, ratio):
        """
        Downsample images by a factor of ratio
        """
        gym.ObservationWrapper.__init__(self, env)
        (oldh, oldw, oldc) = env.observation_space.shape
        newshape = (oldh // ratio, oldw // ratio, oldc)
        self.observation_space = gym.spaces.Box(low=0, high=255,
                                                shape=newshape, dtype=np.uint8)

    def observation(self, frame):
        height, width, _ = self.observation_space.shape
        frame = cv2.resize(frame, (width, height), interpolation=cv2.INTER_AREA)
        if frame.ndim == 2:
            frame = frame[:, :, None]
        return frame


class Rgb2gray(gym.ObservationWrapper):
    def __init__(self, env):
        """
        Downsample images by a factor of ratio
        """
        gym.ObservationWrapper.__init__(self, env)
        (oldh, oldw, _oldc) = env.observation_space.shape
        self.observation_space = gym.spaces.Box(low=0, high=255,
                                                shape=(oldh, oldw, 1), dtype=np.uint8)

    def observation(self, frame):
        frame = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
        return frame[:, :, None]


class MovieRecord(gym.Wrapper):
    def __init__(self, env, savedir, k):
        gym.Wrapper.__init__(self, env)
        self.savedir = savedir
        self.k = k
        self.epcount = 0

    def reset(self):
        if self.epcount % self.k == 0:
            self.env.unwrapped.movie_path = self.savedir
        else:
            self.env.unwrapped.movie_path = None
            self.env.unwrapped.movie = None
        self.epcount += 1
        return self.env.reset()


class AppendTimeout(gym.Wrapper):
    def __init__(self, env):
        gym.Wrapper.__init__(self, env)
        self.action_space = env.action_space
        self.timeout_space = gym.spaces.Box(low=np.array([0.0]), high=np.array([1.0]), dtype=np.float32)
        self.original_os = env.observation_space
        if isinstance(self.original_os, gym.spaces.Dict):
            import copy
            ordered_dict = copy.deepcopy(self.original_os.spaces)
            ordered_dict['value_estimation_timeout'] = self.timeout_space
            self.observation_space = gym.spaces.Dict(ordered_dict)
            self.dict_mode = True
        else:
            self.observation_space = gym.spaces.Dict({
                'original': self.original_os,
                'value_estimation_timeout': self.timeout_space
            })
            self.dict_mode = False
        self.ac_count = None
        while 1:
            if not hasattr(env, "_max_episode_steps"):  # Looking for TimeLimit wrapper that has this field
                env = env.env
                continue
            break
        self.timeout = env._max_episode_steps

    def step(self, ac):
        self.ac_count += 1
        ob, rew, done, info = self.env.step(ac)
        return self._process(ob), rew, done, info

    def reset(self):
        self.ac_count = 0
        return self._process(self.env.reset())

    def _process(self, ob):
        fracmissing = 1 - self.ac_count / self.timeout
        if self.dict_mode:
            ob['value_estimation_timeout'] = fracmissing
        else:
            return {'original': ob, 'value_estimation_timeout': fracmissing}


class StartDoingRandomActionsWrapper(gym.Wrapper):
    """
    Warning: can eat info dicts, not good if you depend on them
    """

    def __init__(self, env, max_random_steps, on_startup=True, every_episode=False):
        gym.Wrapper.__init__(self, env)
        self.on_startup = on_startup
        self.every_episode = every_episode
        self.random_steps = max_random_steps
        self.last_obs = None
        if on_startup:
            self.some_random_steps()

    def some_random_steps(self):
        self.last_obs = self.env.reset()
        n = np.random.randint(self.random_steps)
        # print("running for random %i frames" % n)
        for _ in range(n):
            self.last_obs, _, done, _ = self.env.step(self.env.action_space.sample())
            if done: self.last_obs = self.env.reset()

    def reset(self):
        return self.last_obs

    def step(self, a):
        self.last_obs, rew, done, info = self.env.step(a)
        if done:
            self.last_obs = self.env.reset()
            if self.every_episode:
                self.some_random_steps()
        return self.last_obs, rew, done, info


def make_retro(*, game, state=None, max_episode_steps=4500, **kwargs):
    import retro
    if state is None:
        state = retro.State.DEFAULT
    env = retro.make(game, state, **kwargs)
    env = StochasticFrameSkip(env, n=4, stickprob=0.25)
    if max_episode_steps is not None:
        env = TimeLimit(env, max_episode_steps=max_episode_steps)
    return env


def wrap_deepmind_retro(env, scale=True, frame_stack=4):
    """
    Configure environment for retro games, using config similar to DeepMind-style Atari in wrap_deepmind
    """
    env = WarpFrame(env)
    env = ClipRewardEnv(env)
    if frame_stack > 1:
        env = FrameStack(env, frame_stack)
    if scale:
        env = ScaledFloatFrame(env)
    return env


class Discretizer(gym.ActionWrapper):
    """
    Wrap a gym environment and make it use discrete actions.
    Args:
        combos: ordered list of lists of valid button combinations
    """

    def __init__(self, env, combos):
        super().__init__(env)
        assert isinstance(env.action_space, gym.spaces.MultiBinary)
        buttons = env.unwrapped.buttons
        self._decode_discrete_action = []
        for combo in combos:
            arr = np.array([False] * env.action_space.n)
            for button in combo:
                arr[buttons.index(button)] = True
            self._decode_discrete_action.append(arr)

        self.action_space = gym.spaces.Discrete(len(self._decode_discrete_action))

    def action(self, act):
        return self._decode_discrete_action[act].copy()


class PongDiscretizer(Discretizer):
    """
    Use Sonic-specific discrete actions
    based on https://github.com/openai/retro-baselines/blob/master/agents/sonic_util.py
    """

    def __init__(self, env):
        super().__init__(env=env, combos=[[], ['UP'], ['DOWN']])


class RewardScaler(gym.RewardWrapper):
    """
    Bring rewards to a reasonable scale for PPO.
    This is incredibly important and effects performance
    drastically.
    """

    def __init__(self, env, scale=0.01):
        super(RewardScaler, self).__init__(env)
        self.scale = scale

    def reward(self, reward):
        return reward * self.scale


class AllowBacktracking(gym.Wrapper):
    """
    Use deltas in max(X) as the reward, rather than deltas
    in X. This way, agents are not discouraged too heavily
    from exploring backwards if there is no way to advance
    head-on in the level.
    """

    def __init__(self, env):
        super(AllowBacktracking, self).__init__(env)
        self._cur_x = 0
        self._max_x = 0

    def reset(self, **kwargs):  # pylint: disable=E0202
        self._cur_x = 0
        self._max_x = 0
        return self.env.reset(**kwargs)

    def step(self, action):  # pylint: disable=E0202
        obs, rew, done, info = self.env.step(action)
        self._cur_x += rew
        rew = max(0, self._cur_x - self._max_x)
        self._max_x = max(self._max_x, self._cur_x)
        return obs, rew, done, info



"""#DQN"""


class PongCnn(nn.Module):
    def __init__(self, num_action):
        super(PongCnn, self).__init__()

        Conv = nn.Conv2d

        self.features = nn.Sequential(
            Conv(4, 32, kernel_size=8, stride=4),
            nn.ReLU(),
            Conv(32, 64, kernel_size=4, stride=2),
            nn.ReLU(),
            Conv(64, 64, kernel_size=3, stride=1),
            nn.ReLU(),
        )

        self.fc = nn.Sequential(
            nn.Linear(self.feature_size(), 512),
            nn.ReLU(),
            nn.Linear(512, num_action)
        )

    def forward(self, x):
        x = self.features(x)
        x = x.view(x.size(0), -1)
        x = self.fc(x)
        return x

    def feature_size(self):
        return self.features(torch.zeros(1, 4, 84, 84)).view(1, -1).size(1)


"""#Memory

##Prioritized"""


class SumTree():
    """
    Stores the priorities in sum-tree structure for effecient sampling.
    Tree structure and array storage:
    Tree index:
         0         -> storing priority sum
        / \
      1     2
     / \   / \
    3   4 5   6    -> storing priority for transitions
    Array type for storing:
    [0,1,2,3,4,5,6]
    """

    def __init__(self, capacity):
        self.capacity = capacity  # for all priority values
        self.tree = np.zeros(2 * capacity - 1)
        # [--------------parent nodes-------------][-------leaves to record priority-------]
        #             size: capacity - 1                       size: capacity

    def update(self, idx, p):
        """
        input: idx - id of leaf to update, int
        input: p - new priority value
        """
        assert idx < self.capacity, "SumTree overflow"

        idx += self.capacity - 1  # going to leaf №i

        change = p - self.tree[idx]
        self.tree[idx] = p
        while idx != 0:  # faster than the recursive loop
            idx = (idx - 1) // 2
            self.tree[idx] += change

    def get_leaf(self, v):
        """
        input: v - cumulative priority of first i leafs
        output: i
        """
        parent_idx = 0
        while True:
            cl_idx = 2 * parent_idx + 1  # this leaf's left and right kids
            cr_idx = cl_idx + 1
            if cl_idx >= len(self.tree):  # reach bottom, end search
                leaf_idx = parent_idx
                break
            else:  # downward search, always search for a higher priority node
                if v <= self.tree[cl_idx] or self.tree[cr_idx] == 0.0:
                    parent_idx = cl_idx
                else:
                    v -= self.tree[cl_idx]
                    parent_idx = cr_idx

        return leaf_idx - (self.capacity - 1)

    def __getitem__(self, indices):
        return self.tree[indices + self.capacity - 1]

    @property
    def total_p(self):
        return self.tree[0]  # the root is sum of all priorities


class PrioritizedBufferAgent():  # TODO check if sumtree works faster
    """
    Prioritized replay memory based on weighted importance sampling.
    Proxy of priority is considered to be loss on given transition. For DQN it is absolute of td loss.
    Based on: https://arxiv.org/abs/1511.05952

    Args:
        rp_alpha - degree of prioritezation, float, from 0 to 1
        rp_beta_start - degree of importance sampling smoothing out the bias, float, from 0 to 1
        rp_beta_frames - number of frames till unbiased sampling
        clip_priorities - clipping priorities as suggested in original paper
    """

    def __init__(self, capacity=250000, opt_init=1000, batch_size=32, rp_alpha=0.7, rp_beta_start=0.4,
                 rp_beta_frames=100000):

        self.capacity = capacity
        self.opt_init = opt_init
        self.batch_size = batch_size
        self.memory = []
        self.push_count = 1
        self.pos = 0

        assert self.opt_init >= self.batch_size, "Batch size must be smaller than replay_buffer_init!"

        self.clip_priorities = 1
        self.rp_alpha = rp_alpha
        self.rp_beta_start = rp_beta_start
        self.rp_beta_frames = rp_beta_frames

        self.priorities = SumTree(self.capacity)  # np.array([])
        self.max_priority = 1.0
        self.min_priority = 1.0

        self.rp_beta_by_frame = lambda frame_idx: min(1.0,
                                                      self.rp_beta_start + frame_idx * (
                                                                  1.0 - self.rp_beta_start) / self.rp_beta_frames)

    def memorize_transition(self, state, action, reward, next_state, done):
        state = np.expand_dims(state, 0)
        next_state = np.expand_dims(next_state, 0)

        # this turned out to be the quickest way of working with experience memory
        if len(self.memory) < self.capacity:
            self.memory.append((state, action, reward, next_state, done))
        else:
            self.memory.pop(0)
            self.memory.append((state, action, reward, next_state, done))
            # self.memory[self.pos] = (state, action, reward, next_state, done)
        self.pos = (self.pos + 1) % self.capacity
        self.push_count += 1

    def memorize(self, state, action, reward, next_state, done):

        state = np.expand_dims(state, 0)
        next_state = np.expand_dims(next_state, 0)

        reward = np.float32(reward)

        for s, a, r, ns, d in zip(state, [action], [reward], next_state, [done]):
            self.priorities.update(self.pos, self.max_priority)
            self.memorize_transition(s, a, r, ns, d)
        '''
            state      = np.expand_dims(state, 0)
            next_state = np.expand_dims(next_state, 0)

            # this turned out to be the quickest way of working with experience memory
            if len(self.memory) < self.capacity:
                self.memory.append((state, action, reward, next_state, done))
            else:
                self.memory[self.pos] = (state, action, reward, next_state, done)
        self.pos = (self.pos + 1) % self.capacity
        self.push_count += 1
        '''

    def sample(self):
        # sample batch_size indices
        self.batch_indices = np.array(
            [self.priorities.get_leaf(np.random.uniform(0, self.priorities.total_p)) for _ in range(self.batch_size)])

        # get transitions with these indices
        samples = [self.memory[idx] for idx in self.batch_indices]  # seems like the fastest code for sampling!

        # get priorities of these transitions
        batch_priorities = self.priorities[
            self.batch_indices]  # such indexing is correct for our sumtree implementation

        # calculating importance sampling weights to evade bias
        # these weights are annealed to be more like uniform at the beginning of learning
        weights = (batch_priorities) ** (-self.rp_beta_by_frame(self.push_count))
        # these weights are normalized as proposed in the original article to make loss function scale more stable.
        weights /= batch_priorities.min() ** (-self.rp_beta_by_frame(self.push_count))

        state, action, reward, next_state, done = zip(*samples)

        t1 = Tensor(np.concatenate(state))
        t2 = LongTensor(action)  # torch.LongTensor(action)
        t3 = Tensor(reward)
        t4 = Tensor(np.concatenate(next_state))
        t5 = Tensor(done)

        return (t1, t2, t3, t4, t5)

    def update_priorities(self, batch_priorities):
        """
        Updates priorities for previously sampled batch, indexes stored in self.batch_indices
        input: batch_priorities - np.array, floats, (batch_size)
        """
        new_batch_priorities = (batch_priorities ** self.rp_alpha).clip(min=1e-5, max=self.clip_priorities)
        for i, v in zip(self.batch_indices, new_batch_priorities):
            self.priorities.update(i, v)
        self.max_priority = max(self.max_priority, new_batch_priorities.max())
        self.min_priority = min(self.min_priority, new_batch_priorities.min())

    def can_provide_sample(self):
        return len(self.memory) >= self.opt_init


"""#Agent"""


class Agent():
    def __init__(self, env, policy_net, target_net, memory, gamma, optimizer, device):
        self.current_step = 0
        self.env = env
        self.policy_net = policy_net
        self.target_net = target_net
        self.memory = memory
        self.gamma = gamma
        self.optimizer = optimizer
        self.device = device

        self.score = 0
        self.step_for_episode = 0

        self.is_first = True

        assert self.gamma > 0 and self.gamma <= 1, "Gamma must lie in (0, 1]"

    def select_action(self, state):

        self.current_step += 1

        with torch.no_grad():
            state = np.expand_dims(state, 0)
            # state = state.type(torch.float)
            return self.policy_net(Tensor(state)).argmax(dim=1).to(device)  # exploit .cpu().numpy()

    def gather(self, output, action_b):

        '''
        Returns output of net for given batch of actions
        input: output - FloatTensor (in format of this head's forward function output)
        input: action_b - LongTensor (batch_size)
        output: FloatTensor, head's forward function output for selected actions
        '''
        return output.gather(1, action_b.unsqueeze(1)).squeeze(1)

    def estimate_next_state(self, next_state_b):
        chosen_actions = self.policy_net(next_state_b).argmax(dim=1).to(self.device)
        # chosen_actions = self.policy_net.greedy(self.policy_net(next_state_b))
        return self.gather(self.target_net(next_state_b), chosen_actions)

    def batch_target(self, reward_b, next_state_b, done_b):
        '''
        Calculates target for batch to learn
        input: reward_batch - FloatTensor, (batch_size)
        input: next_state_batch - FloatTensor, (batch_size x state_dim)
        input: done_batch - FloatTensor, (batch_size)
        output: FloatTensor, (batch_size)
        '''
        next_q_values = self.estimate_next_state(next_state_b)
        return reward_b + self.gamma * next_q_values * (1 - done_b)

    def get_loss(self, guess, q):
        '''
        Calculates batch loss
        input: guess - target, FloatTensor, (batch_size)
        input: q - current model output, FloatTensor, (batch_size)
        output: FloatTensor, (batch_size)
        '''
        return (guess - q).pow(2)

    def get_transition_importance(self, loss_b):
        '''
        Calculates importance of transitions in batch by loss
        input: loss_b - FloatTensor, (batch_size)
        output: FloatTensor, (batch_size)
        '''
        return loss_b ** 0.5

    def optimize(self, loss):
        '''
        Make one step of stochastic gradient optimization
        input: loss - Tensor, (1)
        '''
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

        loss_data.append(loss.detach().cpu().numpy())

    def get_transition_importance(self, loss_b):
        '''
        Calculates importance of transitions in batch by loss
        input: loss_b - FloatTensor, (batch_size)
        output: FloatTensor, (batch_size)
        '''
        return loss_b ** 0.5

    def optimize_model(self):
        states, actions, rewards, next_states, dones = memory.sample()

        # getting q values for state and next state
        self.policy_net.train()
        q_values = self.gather(self.policy_net(states), actions)
        with torch.no_grad():
            target_q_values = self.batch_target(rewards, next_states, dones)

        # getting loss
        loss_b = self.get_loss(target_q_values, q_values)

        # updating transition importances
        self.memory.update_priorities(self.get_transition_importance(loss_b).detach().cpu().numpy())

        # making optimization step
        loss = loss_b.mean()
        self.optimize(loss)

    def unfreeze(self):
        '''copy policy net weights to target net'''
        self.target_net.load_state_dict(self.policy_net.state_dict())


    def learn(self, obs, action, rew, next_obs, env_done):


        self.memory.memorize(obs, action[0], rew, next_obs, env_done)
        '''
        if rew == 1:
            rew = -1

        if rew == -1:
            rew = 1

        #plt.imshow(obs[0].T)

        obs[0] = np.flipud(obs[0])
        obs[1] = np.flipud(obs[1])
        obs[2] = np.flipud(obs[2])
        obs[3] = np.flipud(obs[3])

        #plt.imshow(obs[0].T)


        next_obs[0] = np.flipud(next_obs[0])
        next_obs[1] = np.flipud(next_obs[1])
        next_obs[2] = np.flipud(next_obs[2])
        next_obs[3] = np.flipud(next_obs[3])


        self.memory.memorize(obs, action[1], rew, next_obs, env_done)
        '''
        if (rew != 0) and (self.current_step > opt_init):
            self.optimize_model()

            if self.current_step % target_update == 0:
                self.unfreeze()

            if env_done:
                torch.save(self.policy_net.state_dict(), 'agentEXP/AI_RealTime.pt')



    def combineAct(self, actAI, actHum):
        if actHum == 0:
            if actAI == 0: #noop noop
                return 91
            elif actAI == 1: #noop up
                return 21
            else:
                return 166 #noop down
        elif actHum == 1:
            if actAI == 0: #up noop
                return 241
            elif actAI == 1: #up up
                return 99
            else:
                return 317  #up down
        else:
            if actAI == 0: #down noop
                return 31
            elif actAI == 1: #down up
                return 69
            else:
                return 107  #down down

    def display_arr(self, screen, arr, video_size, transpose):
        arr_min, arr_max = arr.min(), arr.max()
        arr = 255.0 * (arr - arr_min) / (arr_max - arr_min)
        pyg_img = pygame.surfarray.make_surface(arr.swapaxes(0, 1) if transpose else arr)
        pyg_img = pygame.transform.scale(pyg_img, video_size)
        screen.blit(pyg_img, (0, 0))

    def play(self, transpose=True, fps=15, zoom=None, callback=None, keys_to_action=None, recording=False):
        self.env.reset()
        rendered = self.env.render(mode='rgb_array')

        if keys_to_action is None:
            if hasattr(self.env, 'get_keys_to_action'):
                keys_to_action = self.env.get_keys_to_action()
            elif hasattr(self.env.unwrapped, 'get_keys_to_action'):
                keys_to_action = self.env.unwrapped.get_keys_to_action()
            else:
                assert False, self.env.spec.id + " does not have explicit key to action mapping, " + \
                              "please specify one manually"
        relevant_keys = set(sum(map(list, keys_to_action.keys()), []))

        video_size = [rendered.shape[1], rendered.shape[0]]
        if zoom is not None:
            video_size = int(video_size[0] * zoom), int(video_size[1] * zoom)

        pressed_keys = []
        running = True
        env_done = True

        screen = pygame.display.set_mode(video_size)
        clock = pygame.time.Clock()

        experiences = []

        gamecounter = 0

        while running:
            if env_done:
                if recording:
                    if gamecounter != 0:
                        np.save('HumanGames/HumanVsAIPongRetro-' + str(gamecounter) + '.npy', experiences)
                        experiences = []

                    gamecounter += 1
                env_done = False
                self.obs = self.env.reset()

            else:
                actAI = self.select_action(self.obs)

                actHum = keys_to_action.get(tuple(sorted(pressed_keys)), 0)

                action = self.combineAct(actAI, actHum)

                self.prev_obs = self.obs

                self.obs, rew, env_done, info = env.step(action)

                if recording:
                    exp = (self.prev_obs, actHum, rew, self.obs, env_done)
                    experiences.append(exp)

                if callback is not None:
                    callback(self.prev_obs, [actAI, actHum], rew, self.obs, env_done)
            if self.obs is not None:
                rendered = env.render(mode='rgb_array')
                self.display_arr(screen, rendered, transpose=transpose, video_size=video_size)

            # process pygame events
            for event in pygame.event.get():
                # test events, set key states
                if event.type == pygame.KEYDOWN:
                    if event.key in relevant_keys:
                        pressed_keys.append(event.key)
                    elif event.key == 27:
                        running = False
                elif event.type == pygame.KEYUP:
                    if event.key in relevant_keys:
                        pressed_keys.remove(event.key)
                elif event.type == pygame.QUIT:
                    running = False
                elif event.type == VIDEORESIZE:
                    video_size = event.size
                    screen = pygame.display.set_mode(video_size)
                    print(video_size)

            pygame.display.flip()
            clock.tick(fps)
        pygame.quit()
        exit()


"""#Main

#hyperparams
"""

eps_start = 1.0
eps_end = 0.01
eps_decay = 30000
memory_size = 180000
opt_init = 1000#10000
batch_size = 32
gamma = 0.99
lr = 0.0001
target_update = 1000

loading = True
checkpointFile = 'checkpoint_1000s.pt'

prioritizedMemory = False
rp_alpha = 0.7
rp_beta_start = 0.4
rp_beta_frames = 100000

human_exp = False

eps_data = []
scores_data = []
timestep_data = []
step_for_episode_data = []
loss_data = []

USE_CUDA = torch.cuda.is_available()
Tensor = lambda *args, **kwargs: torch.FloatTensor(*args, **kwargs).cuda() if USE_CUDA else torch.FloatTensor(*args,
                                                                                                              **kwargs)
LongTensor = lambda *args, **kwargs: torch.LongTensor(*args, **kwargs).cuda() if USE_CUDA else torch.LongTensor(*args,
                                                                                                                **kwargs)

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

env = make_retro(game='Pong-Atari2600', players=2, max_episode_steps=None,
                 use_restricted_actions=retro.Actions.DISCRETE)
# env = PongDiscretizer(env)
env = wrap_deepmind_retro(env)
env = ImageToPyTorch(env)


target_net = PongCnn(3).to(device) #
policy_net = PongCnn(3).to(device)  # env.action_space.n
try:
    checkpoint = torch.load('agentEXP/checkpoint_1000_humExp_20.pt')#, map_location=torch.device('cpu'))
    policy_net.load_state_dict(checkpoint['policyNet_state_dict'])
except FileNotFoundError:
    print('//-----------------------//')
    print('Agent experience not found!')
    print('//-----------------------//')

policy_net.eval()
optimizer = optim.Adam(params=policy_net.parameters(), lr=lr)



memory = PrioritizedBufferAgent(memory_size, opt_init, batch_size)
agent = Agent(env, policy_net, target_net, memory, gamma, optimizer, device)

"""##Setting"""


def run(op, train, record): #main
    actions = {
        (ord('w'),): 1,
        (ord('s'),): 2
    }

    if op == 'AI':
        if train:
            agent.play(zoom=3, fps=15, keys_to_action=actions, callback=agent.learn, recording=record)
        else:
            agent.play(zoom=3, fps=15, keys_to_action=actions, recording=record)
        env.close()
    else:
        env.close()
        cpu_game.main(record)


#if __name__ == "__main__":
#    main()

