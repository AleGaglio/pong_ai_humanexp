from tqdm import tqdm
import retro
import torch
import torch.nn as nn

"""#Retro_wrapper"""

from collections import deque
import cv2

cv2.ocl.setUseOpenCL(False)
# from .atari_wrappers import WarpFrame, ClipRewardEnv, FrameStack, ScaledFloatFrame
# from .wrappers import TimeLimit
import numpy as np
import gym
from gym.wrappers.time_limit import TimeLimit
from matplotlib import pyplot as plt


class LazyFrames(object):
    def __init__(self, frames):
        """This object ensures that common frames between the observations are only stored once.
        It exists purely to optimize memory usage which can be huge for DQN's 1M frames replay
        buffers.
        This object should only be converted to numpy array before being passed to the model.
        You'd not believe how complex the previous solution was."""
        self._frames = frames
        self._out = None

    def _force(self):
        if self._out is None:
            self._out = np.concatenate(self._frames, axis=-1)
            self._frames = None
        return self._out

    def __array__(self, dtype=None):
        out = self._force()
        if dtype is not None:
            out = out.astype(dtype)
        return out

    def __len__(self):
        return len(self._force())

    def __getitem__(self, i):
        return self._force()[i]


class ImageToPyTorch(gym.ObservationWrapper):
    """
    Image shape to num_channels x weight x height
    """

    def __init__(self, env):
        super(ImageToPyTorch, self).__init__(env)
        old_shape = self.observation_space.shape
        self.observation_space = gym.spaces.Box(low=0.0, high=1.0, shape=(old_shape[-1], old_shape[0], old_shape[1]),
                                                dtype=np.uint8)

    def observation(self, observation):
        return np.swapaxes(observation, 2, 0)


class FrameStack(gym.Wrapper):
    def __init__(self, env, k):
        """Stack k last frames.
        Returns lazy array, which is much more memory efficient.
        See Also
        --------
        baselines.common.atari_wrappers.LazyFrames
        """
        gym.Wrapper.__init__(self, env)
        self.k = k
        self.frames = deque([], maxlen=k)
        shp = env.observation_space.shape
        self.observation_space = gym.spaces.Box(low=0, high=255, shape=(shp[:-1] + (shp[-1] * k,)),
                                                dtype=env.observation_space.dtype)

    def reset(self):
        ob = self.env.reset()
        for _ in range(self.k):
            self.frames.append(ob)
        return self._get_ob()

    def step(self, action):
        ob, reward, done, info = self.env.step(action)
        self.frames.append(ob)
        return self._get_ob(), reward, done, info

    def _get_ob(self):
        assert len(self.frames) == self.k
        return LazyFrames(list(self.frames))


class ScaledFloatFrame(gym.ObservationWrapper):
    def __init__(self, env):
        gym.ObservationWrapper.__init__(self, env)
        self.observation_space = gym.spaces.Box(low=0, high=1, shape=env.observation_space.shape, dtype=np.float32)

    def observation(self, observation):
        # careful! This undoes the memory optimization, use
        # with smaller replay buffers only.
        return np.array(observation).astype(np.float32) / 255.0


class WarpFrame(gym.ObservationWrapper):
    def __init__(self, env, width=84, height=84, grayscale=True):
        """Warp frames to 84x84 as done in the Nature paper and later work."""
        gym.ObservationWrapper.__init__(self, env)
        self.width = width
        self.height = height
        self.grayscale = grayscale
        if self.grayscale:
            self.observation_space = gym.spaces.Box(low=0, high=255,
                                                    shape=(self.height, self.width, 1), dtype=np.uint8)
        else:
            self.observation_space = gym.spaces.Box(low=0, high=255,
                                                    shape=(self.height, self.width, 3), dtype=np.uint8)

    def observation(self, frame):
        if self.grayscale:
            frame = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
        frame = cv2.resize(frame, (self.width, self.height), interpolation=cv2.INTER_AREA)
        if self.grayscale:
            frame = np.expand_dims(frame, -1)
        return frame


class ClipRewardEnv(gym.RewardWrapper):
    def __init__(self, env):
        gym.RewardWrapper.__init__(self, env)

    def reward(self, reward):
        """Bin reward to {+1, 0, -1} by its sign."""
        return np.sign(reward)


class StochasticFrameSkip(gym.Wrapper):
    def __init__(self, env, n, stickprob):
        gym.Wrapper.__init__(self, env)
        self.n = n
        self.stickprob = stickprob
        self.curac = None
        self.rng = np.random.RandomState()
        self.supports_want_render = hasattr(env, "supports_want_render")

    def reset(self, **kwargs):
        self.curac = None
        return self.env.reset(**kwargs)

    def step(self, ac):
        done = False
        totrew = 0
        for i in range(self.n):
            # First step after reset, use action
            if self.curac is None:
                self.curac = ac
            # First substep, delay with probability=stickprob
            elif i == 0:
                if self.rng.rand() > self.stickprob:
                    self.curac = ac
            # Second substep, new action definitely kicks in
            elif i == 1:
                self.curac = ac
            if self.supports_want_render and i < self.n - 1:
                ob, rew, done, info = self.env.step(self.curac, want_render=False)
            else:
                ob, rew, done, info = self.env.step(self.curac)
            totrew += rew[0]
            if done: break
        return ob, totrew, done, info

    def seed(self, s):
        self.rng.seed(s)


class Downsample(gym.ObservationWrapper):
    def __init__(self, env, ratio):
        """
        Downsample images by a factor of ratio
        """
        gym.ObservationWrapper.__init__(self, env)
        (oldh, oldw, oldc) = env.observation_space.shape
        newshape = (oldh // ratio, oldw // ratio, oldc)
        self.observation_space = gym.spaces.Box(low=0, high=255,
                                                shape=newshape, dtype=np.uint8)

    def observation(self, frame):
        height, width, _ = self.observation_space.shape
        frame = cv2.resize(frame, (width, height), interpolation=cv2.INTER_AREA)
        if frame.ndim == 2:
            frame = frame[:, :, None]
        return frame


class Rgb2gray(gym.ObservationWrapper):
    def __init__(self, env):
        """
        Downsample images by a factor of ratio
        """
        gym.ObservationWrapper.__init__(self, env)
        (oldh, oldw, _oldc) = env.observation_space.shape
        self.observation_space = gym.spaces.Box(low=0, high=255,
                                                shape=(oldh, oldw, 1), dtype=np.uint8)

    def observation(self, frame):
        frame = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
        return frame[:, :, None]


class MovieRecord(gym.Wrapper):
    def __init__(self, env, savedir, k):
        gym.Wrapper.__init__(self, env)
        self.savedir = savedir
        self.k = k
        self.epcount = 0

    def reset(self):
        if self.epcount % self.k == 0:
            self.env.unwrapped.movie_path = self.savedir
        else:
            self.env.unwrapped.movie_path = None
            self.env.unwrapped.movie = None
        self.epcount += 1
        return self.env.reset()


def make_retro(*, game, state=None, max_episode_steps=4500, **kwargs):
    import retro
    if state is None:
        state = retro.State.DEFAULT
    env = retro.make(game, state, **kwargs)
    env = StochasticFrameSkip(env, n=4, stickprob=0.25)
    if max_episode_steps is not None:
        env = TimeLimit(env, max_episode_steps=max_episode_steps)
    return env


def wrap_deepmind_retro(env, scale=True, frame_stack=4):
    """
    Configure environment for retro games, using config similar to DeepMind-style Atari in wrap_deepmind
    """
    env = WarpFrame(env)
    env = ClipRewardEnv(env)
    if frame_stack > 1:
        env = FrameStack(env, frame_stack)
    if scale:
        env = ScaledFloatFrame(env)
    return env


class Discretizer(gym.ActionWrapper):
    """
    Wrap a gym environment and make it use discrete actions.
    Args:
        combos: ordered list of lists of valid button combinations
    """

    def __init__(self, env, combos):
        super().__init__(env)
        assert isinstance(env.action_space, gym.spaces.MultiBinary)
        buttons = env.unwrapped.buttons
        self._decode_discrete_action = []
        # print(buttons)
        buttons = []
        buttons.append(['BUTTON', None, 'SELECT', 'RESET',
                        'UP', 'DOWN', 'LEFT', 'RIGHT'])
        buttons.append(['UP', 'DOWN', 'SELECT', 'RESET', 'NONSENSE',
                        None, 'LEFT', 'RIGHT', 'NONSENSE',
                        'BUTTON'])  # I could only make sure that the 7th and 8th digit is UP and DOWN for player2.

        for combo in combos:
            arr = np.array([False] * env.action_space.n)
            # print(arr)
            for i, button_two in enumerate(combo):
                if len(button_two) == 0:
                    arr[6 * i + buttons[i].index(None)] = True

                for button in button_two:
                    arr[6 * i + buttons[i].index(button)] = True
            self._decode_discrete_action.append(arr.copy())
            # print((combo, arr, len(self._decode_discrete_action)))
        self.action_space = gym.spaces.Discrete(len(self._decode_discrete_action))

    def action(self, act):
        return self._decode_discrete_action[act].copy()


class PongDiscretizer(Discretizer):
    """
    Use Sonic-specific discrete actions
    based on https://github.com/openai/retro-baselines/blob/master/agents/sonic_util.py
    """

    def __init__(self, env, players=2):
        lens1 = [[], ['BUTTON'], ['UP'], ['DOWN'],
                 ['BUTTON', 'UP'], ['BUTTON', 'DOWN']]
        lens2 = [[], ['BUTTON'], ['UP'], ['DOWN'], ['RESET'], ['SELECT']]
        combos = []
        for i in range(len(lens1)):
            for j in range(len(lens2)):
                combos.append((lens1[i], lens2[j]))
        super().__init__(env=env, combos=combos)


class RewardScaler(gym.RewardWrapper):
    """
    Bring rewards to a reasonable scale for PPO.
    This is incredibly important and effects performance
    drastically.
    """

    def __init__(self, env, scale=0.01):
        super(RewardScaler, self).__init__(env)
        self.scale = scale

    def reward(self, reward):
        return reward * self.scale


"""#DQN"""


class PongCnn(nn.Module):
    def __init__(self, num_action):
        super(PongCnn, self).__init__()

        Conv = nn.Conv2d

        self.features = nn.Sequential(
            Conv(4, 32, kernel_size=8, stride=4),
            nn.ReLU(),
            Conv(32, 64, kernel_size=4, stride=2),
            nn.ReLU(),
            Conv(64, 64, kernel_size=3, stride=1),
            nn.ReLU(),
        )

        self.fc = nn.Sequential(
            nn.Linear(self.feature_size(), 512),
            nn.ReLU(),
            nn.Linear(512, num_action)
        )

    def forward(self, x):
        x = self.features(x)
        x = x.view(x.size(0), -1)
        x = self.fc(x)
        return x

    def feature_size(self):
        return self.features(torch.zeros(1, 4, 84, 84)).view(1, -1).size(1)


"""#Agent"""


class Agent():
    def __init__(self, policy_net, device):
        self.policy_net = policy_net
        self.device = device

    def select_action(self, state):
        with torch.no_grad():
            state = np.expand_dims(state, 0)
            # state = state.type(torch.float)
            return self.policy_net(Tensor(state)).argmax(dim=1).to(device)


def combineAct(actAI1, actAI2):
    if actAI2 == 0:
        if actAI1 == 0:  # noop noop
            act = 7
            # act = [1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1]
        elif actAI1 == 1:  # noop up
            act = 25
            # act = [1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0]
        elif actAI1 == 2:
            act = 30  # noop down
            # act = [1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0]
    elif actAI2 == 1:
        if actAI1 == 0:  # up noop
            act = 8
            # act = [1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1]
        elif actAI1 == 1:  # up up
            act = 26
            # act = [1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0]
        elif actAI1 == 2:
            act = 32  # up down
            # act = [1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1]
    elif actAI2 == 2:
        if actAI1 == 0:  # down noop
            act = 9
            # act = [1, 0, 1, 1, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0]
        elif actAI1 == 1:  # down up
            act = 27
            # act = [0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0]
        elif actAI1 == 2:
            act = 33  # down down
            # act = [1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1]

    return torch.tensor([act]).to(device)


def print_result(games):
    green_wins = 0
    orange_wins = 0
    gc = 0

    for game in games:
        gc += 1
        if game > 0:
            print(gc, '°: Green Wins!')
            green_wins += 1
        else:
            print(gc, '°: Orange Wins!')
            orange_wins += 1

    print('<---------------------------------------->')

    if green_wins > orange_wins:
        print('Green player is the best!')
    elif green_wins < orange_wins:
        print('Orange player is the best!')
    else:
        print('Green & orange players are equivalent!')


"""
    #Main
"""
USE_CUDA = torch.cuda.is_available()
Tensor = lambda *args, **kwargs: torch.FloatTensor(*args, **kwargs).cuda() if USE_CUDA else torch.FloatTensor(*args,
                                                                                                              **kwargs)
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

env = make_retro(game='Pong-Atari2600', players=2, max_episode_steps=3000)
# use_restricted_actions=retro.Actions.DISCRETE)

env = PongDiscretizer(env)
env = wrap_deepmind_retro(env)
env = ImageToPyTorch(env)

policy_net_1 = PongCnn(3).to(device)  # env.action_space.n
policy_net_2 = PongCnn(3).to(device)
try:
    checkpoint_2 = torch.load('agentEXP/checkpointGPU_1000_normal2.pt')  # , map_location=torch.device('cpu'))
    policy_net_2.load_state_dict(checkpoint_2['policyNet_state_dict'])

    checkpoint_1 = torch.load('agentEXP/checkpointGPU_1000_humExp_15.pt')  # , map_location=torch.device('cpu'))
    policy_net_1.load_state_dict(checkpoint_1['policyNet_state_dict'])
except FileNotFoundError:
    print('//-----------------------//')
    print('Agent experience not found!')
    print('//-----------------------//')

policy_net_1.eval()
policy_net_2.eval()

agent_1 = Agent(policy_net_1, device)
agent_2 = Agent(policy_net_2, device)

render = False
games_res = []

i = 0
cp1tot = 0
cp2tot = 0

while True:

    obs = env.reset()
    tot_rew = 0
    cp1 = 0
    cp2 = 0

    if render:
        env.render()

    if cp1tot == 5 or cp2tot == 5:
        break

    print(i + 1)

    while True:

        actAI1 = agent_1.select_action(obs)
        obs[0] = np.flipud(obs[0])
        obs[1] = np.flipud(obs[1])
        obs[2] = np.flipud(obs[2])
        obs[3] = np.flipud(obs[3])
        actAI2 = agent_2.select_action(obs)

        # print('act1: ', actAI1)
        # print('act2: ', actAI2)

        action = combineAct(actAI1, actAI2)
        # print(action)
        # print('meaning: ', env.get_action_meaning(action))
        obs, rew, done, info = env.step(action)
        #if rew != 0:
        #    env.step(3)#3 - 29 - 34
        if rew == 1:
            cp1 += 1
        if rew == -1:
            cp2 += 1
        if render:
            env.render()
        tot_rew += rew
        if done:
            if (cp2 > 20 or cp1 > 20) and (cp1 != cp2):
                if cp2 > cp1:
                    cp1tot += 1
                else:
                    cp1tot += 1
                i += 1
                games_res.append(tot_rew)
            break
print(games_res)

print_result(games_res)
